variable "api_key" {
  description = "API key for the private cloud provider"
  type        = string
  default     = ""
}

variable "lb_internal_ip" {
  description = "internal IP address for LB"
  type        = string
  default     = "10.0.0.4"
}

variable "lb_external_ip" {
  description = "external IP address for LB"
  type        = string
  default     = "167.233.9.120"
}

variable "internal_ip_range" {
  description = "Path to the private key for SSH access"
  type        = string
  default     = "10.0.0."
}

variable "fqdn" {
  description = "Fully Qualified Domain Name for the WordPress cluster"
  type        = string
  default     = "wordpress-ah.example.com"
}

variable "mysql_root_password" {
  description = "Root password for MySQL"
  default     = "password"
}

variable "ssh_key" {
  description = "SSH public key for web server access"
  type        = string
  default     = <<EOT
ssh-rsa  
EOT
}

