provider "terraform" {
  path = "inventory/inventory.tf"
}

## Define the provider
terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
    }
  }
}

## Define Hetzner API key
provider "hcloud" {
  token = var.api_key
}

## Setup SSH key
resource "hcloud_ssh_key" "ssh" {
  name       = "ssh"
  public_key = var.ssh_key
  }

## Setup a private network 
resource "hcloud_network" "private_network" {
  name       = "private_network"
  ip_range   = "10.0.0.0/24"
  labels     = {
  "environment" = "private"
  }
}

## Define a subnet within the private network
resource "hcloud_network_subnet" "private_subnet" {
  network_id    = hcloud_network.private_network.id
  ip_range      = "10.0.0.0/28"  
  type          = "server"
  network_zone  = "eu-central"
}

## Setup database server for the webservers
resource "hcloud_server" "db_server" {
  name         = "db"
  server_type  = "cx21"
  image        = "ubuntu-22.04"
  ssh_keys     = [resource.hcloud_ssh_key.ssh.id]

  network {
    network_id    = hcloud_network.private_network.id
    ip            = "10.0.0.10" 
#}
}
  depends_on = [
    hcloud_network_subnet.private_subnet
  ]
}

## Setup the web servers using a counter
resource "hcloud_server" "webserver" {
  count        = 3
  name         = "web${count.index + 1}"
  server_type  = "cx21"
  image        = "ubuntu-22.04"
  ssh_keys     = [resource.hcloud_ssh_key.ssh.id]

  network {
    network_id    = hcloud_network.private_network.id
    ip            = "10.0.0.${count.index + 2}" 

}
  depends_on = [
    hcloud_network_subnet.private_subnet
  ]
}
## Setup LB server
resource "hcloud_server" "lb_server" {
  count       = 1
  name        = "lb"
  image       = "ubuntu-22.04"
  server_type = "cx21"
  ssh_keys     = [resource.hcloud_ssh_key.ssh.id]
  
  network {
    ip = var.lb_internal_ip
    network_id  = hcloud_network.private_network.id
  }
  depends_on = [
    hcloud_network_subnet.private_subnet
  ]
}