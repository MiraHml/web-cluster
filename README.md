# WordPress Cluster Terraform Module

This Terraform module allows you to create a WordPress cluster on Hetzner Cloud. The cluster consists of multiple web servers and a load balancer, along with a database server for storing the WordPress data. The module sets up the necessary infrastructure and provisions the servers with the required software and configurations.

## Prerequisites

- Terraform installed on your machine ([installation instructions](https://learn.hashicorp.com/tutorials/terraform/install-cli))
- Hetzner Cloud API token ([create API token](https://console.hetzner.cloud/projects))


## Variables

- api_key: (Required) Hetzner Cloud API token used to authenticate with Hetzner Cloud.
- lb_internal_ip: (Required) internal IPv4 for the Load Balancer.
- lb_external_ip: (Required) external IP for the Load Balancer.
- internal_ip_range: (Required) internal IPv4 range for the web servers.
- fqdn: (Required) The fully qualified domain name (FQDN) to associate with the load balancer. This is used for accessing the WordPress cluster.
- ssh_key: (Required) rsa key in the servers.
- mysql_root_password: (Required) The root password for the MySQL database server.

## Usage

Follow the steps below to use this Terraform module:

1. Initialize the Terraform configuration by running the following command:
```
terraform init
```
2. Create an execution plan by running: 
```
terraform plan
```
3. You can customize the cluster configuration by adjusting the variables provided by the module.

4. Apply the Terraform configuration to create the WordPress cluster:
```
terraform apply
```

## Ansible Playbook
Run the ansible playbook as follows to finish setting up the cluster:

```
ansible-playbook playbooks/main.yml -i hosts/hosts.ini
```

Everything now should be ready and the webserver can be accessible via the fqdn set in the variables.